<section class="news-and-events">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="sectiontitle script">Events</h2>
			</div>
		</div>
		<div class="row events-carousel p-0">

			<!--events loop-->
			<?php $loop = new WP_Query( array( 'post_type' => 'tribe_events', 'posts_per_page' => 9 ) ); ?>

			<?php while ( $loop->have_posts() ) : $loop->the_post();
				//use whatever method here serves you
				//background image url on div
				$slide_background = get_the_post_thumbnail_url();
				//actual image element with srcset:
				//$slide_image = the_post_thumbnail(); 
				?>


				<div class="col-12 event-slide" style="background-image: url('<?php echo $slide_background; ?>'); background-size: cover">
					<a class="eventlink" href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
					<?php echo tribe_get_start_date($post->ID, false, 'l F jS - g:i a'); ?> to <?php echo tribe_get_end_date($post->ID, false, 'g:i a'); ?>
					<br /><?php echo tribe_get_venue(); ?> - <?php echo tribe_get_address(); ?>

					<?php echo wp_trim_words( get_the_content(), 20 ); ?>
				</div>


			<?php endwhile; wp_reset_query(); ?>
			<!--end events loop-->


			<a href="<?php echo get_site_url(); ?>/events/" class="btn text-link link-effect btn-warning">All Events</a>

		</div>
	</div>
</section>


<?php


//for functions file
function mysite_customscripts() {
// Add Slick Slider default CSS
wp_enqueue_style( 'slick', get_template_directory_uri() . '/inc/css/slick.css' );
// Add Slick Slider theme CSS
wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/inc/css/slick-theme.css' );
// Add Slick JS
wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/inc/js/slick.min.js', array( 'jquery' ), '20161229', true );
}
add_action( 'wp_enqueue_scripts', 'mysite_customscripts' );

?>



<script>
	//for init in theme project or concat js file
jQuery('.events-carousel).slick({
	infinite: true,
	speed: 300,
	slidesToShow: 1,
	adaptiveHeight: true});
</script>